--Configuration Init
require('conf.options')
require('conf.theme')
require('conf.bindings')
require('conf.completion')
require('conf.ui')
